from django.db import models
from django.forms import ModelForm

# Create your models here.
# nama mata kuliah, dosen pengajar, jumlah sks, deskripsi mata kuliah, semester tahun(contoh: Gasal 2019/2020) dan ruang kelas
class mataKuliah(models.Model):
    
    nama = models.CharField(max_length = 50, help_text = "Enter The Name!")
    dosen = models.CharField(max_length = 50)
    sks = models.IntegerField()
    deskripsi = models.TextField()
    smtTahun = models.CharField(max_length = 50)
    kelas = models.CharField(max_length = 50)

    def __str__(self):
        return self.nama

class mataKuliahForm(ModelForm):
    class Meta:
        model = mataKuliah
        fields = ['nama', 'dosen', 'sks', 'deskripsi', 'smtTahun', 'kelas']

class tugasKuliah(models.Model):

    nama = models.CharField(max_length = 50)
    deadline = models.DateTimeField()
    mataKuliah = models.ForeignKey(
        'homepage.mataKuliah', on_delete=models.CASCADE
    )

    def __str__(self):
        return self.nama
