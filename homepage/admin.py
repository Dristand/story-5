from django.contrib import admin
from .models import mataKuliah, tugasKuliah

# Register your models here.
admin.site.register(mataKuliah)
admin.site.register(tugasKuliah)