from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('addMatkul', views.addMatkul, name='addMatkul'),
    path('delMatkul/<str:nama>', views.delMatkul, name='delMatkul'),
    path('viewMatkul/<str:nama>', views.viewMatkul, name='viewMatkul'),
]