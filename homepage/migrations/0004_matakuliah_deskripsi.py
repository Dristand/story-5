# Generated by Django 3.0.3 on 2020-02-26 08:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0003_auto_20200226_0232'),
    ]

    operations = [
        migrations.AddField(
            model_name='matakuliah',
            name='deskripsi',
            field=models.TextField(default='[Empty Description]'),
            preserve_default=False,
        ),
    ]
