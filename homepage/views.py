from django.shortcuts import render, redirect
from .models import mataKuliah, mataKuliahForm, tugasKuliah
import datetime

# Create your views here.

def index(request):
    matKuls = mataKuliah.objects.all()

    return render(request, 'index.html', {'matKuls': matKuls})

def addMatkul(request):

    if request.method == 'POST':
        form = mataKuliahForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('homepage:index')
    else:
        form = mataKuliahForm()
    

    return render(request, 'addMatkul.html', {'form' : form})

def delMatkul(request, nama):
    matKuls = mataKuliah.objects.filter(nama = nama)

    matKuls.delete()

    return redirect('homepage:index')

def viewMatkul(request, nama):
    matKul = mataKuliah.objects.filter(nama = nama)
    tugas = tugasKuliah.objects.all().order_by('deadline')

    return render(request, 'viewMatkul.html', {'matKul' : matKul, 'tugas2' : tugas.order_by('deadline')})


    # Intended code for time
    # result = []
    # warning1, warning2, warning3 = False, False, False

    # timeSnow = datetime.datetime.now().timestamp()
    # timeSDead  = tugas.deadline.timestamp()

    # dateNow = datetime.datetime.fromtimestamp(timeSnow)
    # dateDead = datetime.datetime.fromtimestamp(timeSDead)

    # if (dateNow.month == dateDead.month):
    #     if (dateNow.day == dateDead.day):
    #         warning3 = True
    #     else:
    #         warning2 = True
    # else:
    #     warning1 = True